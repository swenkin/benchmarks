﻿using System.IO;
using BenchmarkDotNet.Columns;
using BenchmarkDotNet.Reports;
using BenchmarkDotNet.Running;

namespace Benchmark
{
    public class FileSize : IColumn
    {
        public string Id => nameof(FileSize);
        public string ColumnName => "SizeAfterCreation";
        public string Legend => "Allocated memory on disk after clip is created";
        public UnitType UnitType => UnitType.Size;
        public bool AlwaysShow => true;
        public ColumnCategory Category => ColumnCategory.Metric;
        public int PriorityInCategory => 0;
        public bool IsNumeric => true;
        public bool IsAvailable(Summary summary) => true;
        public bool IsDefault(Summary summary, BenchmarkCase benchmarkCase) => false;
        public string GetValue(Summary summary, BenchmarkCase benchmarkCase) => GetValue(summary, benchmarkCase, SummaryStyle.Default);
        public string GetValue(Summary summary, BenchmarkCase benchmarkCase, SummaryStyle style)
        {
            var path = Path.Combine("C:\\", "TEST", "output.mp4");
            if (File.Exists(path))
            {
                FileInfo fi = new FileInfo(path);
                var size = (fi.Length / 1048576).ToString() + "MB";
                return size;
            }
            return "";
        }
        public override string ToString() => ColumnName;
    }
}
