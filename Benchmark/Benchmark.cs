﻿using System.Diagnostics;
using System.IO;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Columns;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Reports;

namespace Benchmark
{
    [RankColumn]
    [Config(typeof(Config))]
    public class Benchmark
    {
        private class Config : ManualConfig
        {
            public Config() => AddColumn(new FileSize());
        }

        [Benchmark]
        public void GenerateClipInitialCommand()
        {
            // Initial command
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -map \"[video]\" -map 2:a -shortest \"C:\\TEST\\GenerateClipInitialCommand.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipCRFAt29()
        {
            Changing CRF
             CRF means Constant Rate Factor.It defines how many bits will be used for each frame of the video.

            This impacts the video quality, as well as its size.The value ranges from 0 to 51(0 = perfect quality, 23 = default, 51 = worst quality).

           var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
               "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
               "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
               "-s 3840:2160 -crf 29 -map \"[video]\" -map 2:a -shortest \"C:\\TEST\\GenerateClipCRFAt29.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipPresetFast()
        {
            // Changing CRF
            // CRF means Constant Rate Factor. It defines how many bits will be used for each frame of the video.
            // This impacts the video quality, as well as its size. The value ranges from 0 to 51 (0 = perfect quality, 23 = default, 51 = worst quality).
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -preset fast -map \"[video]\" -map 2:a -shortest \"C:\\TEST\\GenerateClipPresetFast.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipPresetFaster()
        {
            // Changing preset
            // Preset defines the "encoding speed to compression" ratio. The slower the preset, the better the compression and the slower the encoding.
            // A faster preset provides a bigger video file but encoding takes less time.
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -preset faster -map \"[video]\" -map 2:a -shortest \"C:\\TEST\\GenerateClipPresetFaster.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipPresetVeryFast()
        {
            // Changing preset
            // Preset defines the "encoding speed to compression" ratio. The slower the preset, the better the compression and the slower the encoding.
            // A faster preset provides a bigger video file but encoding takes less time.
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -preset veryfast -map \"[video]\" -map 2:a -shortest \"C:\\TEST\\GenerateClipPresetVeryFast.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipPresetSuperFast()
        {
            // Changing preset
            // Preset defines the "encoding speed to compression" ratio. The slower the preset, the better the compression and the slower the encoding.
            // A faster preset provides a bigger video file but encoding takes less time.
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -preset superfast -map \"[video]\" -map 2:a -shortest \"C:\\TEST\\GenerateClipPresetSuperFast.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipPresetUltraFast()
        {
            // Changing preset
            // Preset defines the "encoding speed to compression" ratio. The slower the preset, the better the compression and the slower the encoding.
            // A faster preset provides a bigger video file but encoding takes less time.
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -preset ultrafast -map \"[video]\" -map 2:a -shortest \"C:\\TEST\\GenerateClipPresetUltraFast.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipCRFAt29PresetFast()
        {
            // Changing CRF and preset
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -crf 29 -preset fast -map \"[video]\" -map 2:a -shortest \"C:\\TEST\\GenerateClipCRFAt29PresetFast.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipCRFAt29PresetFaster()
        {
            // Changing CRF and preset
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -crf 29 -preset faster -map \"[video]\" -map 2:a -shortest \"C:\\TEST\\GenerateClipCRFAt29PresetFaster.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipCRFAt29PresetVeryFast()
        {
            // Changing CRF and preset
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -crf 29 -preset veryfast -map \"[video]\" -map 2:a -shortest \"C:\\TEST\\GenerateClipCRFAt29PresetVeryFast.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipCRFAt29PresetSuperFast()
        {
            // Changing CRF and preset
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -crf 29 -preset superfast -map \"[video]\" -map 2:a -shortest \"C:\\TEST\\GenerateClipCRFAt29PresetSuperFast.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipCRFAt29PresetUltraFast()
        {
            // Changing CRF and preset
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -crf 29 -preset ultrafast -map \"[video]\" -map 2:a -shortest \"C:\\TEST\\GenerateClipCRFAt29PresetUltraFast.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipCRFAt15PresetUltraFast()
        {
            // Changing CRF and preset
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -crf 15 -preset ultrafast -map \"[video]\" -map 2:a -shortest \"C:\\TEST\\GenerateClipCRFAt15PresetUltraFast.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipCRFAt10PresetUltraFast()
        {
            // Changing CRF and preset
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -crf 10 -preset ultrafast -map \"[video]\" -map 2:a -shortest \"C:\\TEST\\GenerateClipCRFAt10PresetUltraFast.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipCRFAt15PresetSuperFast()
        {
            // Changing CRF and preset
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -crf 15 -preset superfast -map \"[video]\" -map 2:a -shortest \"C:\\TEST\\GenerateClipCRFAt15PresetSuperFast.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipCRFAt10PresetSuperFast()
        {
            // Changing CRF and preset
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -crf 10 -preset superfast -map \"[video]\" -map 2:a -shortest \"C:\\TEST\\GenerateClipCRFAt10PresetSuperFast.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipNvencCQAt1()
        {
            // Using h264_nvenc
            // h264_nvenc is a hardware accelerated encoder. It neeeds NVIDIA Cuda driver 11.1 minimum.
            // h264_nvenc AVOptions:
            //  -preset<int>                    E..V....... Set the encoding preset(from 0 to 18) (default p4)
            //     default         0            E..V....... 
            //     slow            1            E..V....... hq 2 passes
            //     medium          2            E..V....... hq 1 pass
            //     fast            3            E..V....... hp 1 pass
            //     hp              4            E..V....... 
            //     hq              5            E..V....... 
            //     bd              6            E..V....... 
            //     ll              7            E..V....... low latency
            //     llhq            8            E..V....... low latency hq
            //     llhp            9            E..V....... low latency hp
            //     lossless        10           E..V....... 
            //     losslesshp      11           E..V....... 
            //     p1              12           E..V....... fastest(lowest quality)
            //     p2              13           E..V....... faster(lower quality)
            //     p3              14           E..V....... fast(low quality)
            //     p4              15           E..V....... medium(default)
            //     p5              16           E..V....... slow(good quality)
            //     p6              17           E..V....... slower(better quality)
            //     p7              18           E..V....... slowest(best quality)
            //  - profile<int>                  E..V....... Set the encoding profile(from 0 to 3) (default main)
            //     baseline        0            E..V....... 
            //     main            1            E..V....... 
            //     high            2            E..V....... 
            //     high444p        3            E..V....... 
            //  -cq<float>                      E..V....... Set target quality level(0 to 51, 0 means automatic) for constant quality mode in VBR rate control(from 0 to 51)(default 0)
            //  - rc<int>                       E..V....... Override the preset rate - control(from - 1 to INT_MAX)(default - 1)
            //     constqp         0            E..V....... Constant QP mode
            //     vbr             1            E..V....... Variable bitrate mode
            //     cbr             2            E..V....... Constant bitrate mode
            //     vbr_minqp       8388612      E..V....... Variable bitrate mode with MinQP(deprecated)
            //     ll_2pass_quality 8388616     E..V....... Multi - pass optimized for image quality (deprecated)
            //     ll_2pass_size   8388624      E..V....... Multi - pass optimized for constant frame size(deprecated)
            //     vbr_2pass       8388640      E..V....... Multi - pass variable bitrate mode(deprecated)
            //     cbr_ld_hq       8388616      E..V....... Constant bitrate low delay high quality mode
            //     cbr_hq          8388624      E..V....... Constant bitrate high quality mode
            //     vbr_hq          8388640      E..V....... Variable bitrate high quality mode

            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -map \"[video]\" -map 2:a -shortest -vcodec h264_nvenc -cq:v 1 \"C:\\TEST\\GenerateClipNvencCQAt1.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipNvencCQAt51()
        {
            // Changes CQ
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -map \"[video]\" -map 2:a -shortest -vcodec h264_nvenc -cq:v 51 \"C:\\TEST\\GenerateClipNvencCQAt51.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipNvencCQAt1PresetDefault()
        {
            // Changes CQ and preset
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -map \"[video]\" -map 2:a -shortest -vcodec h264_nvenc -cq:v 1 -preset:v default \"C:\\TEST\\GenerateClipNvencCQAt1PresetDefault.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipNvencCQAt51PresetDefault()
        {
            // Changes CQ and preset
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -map \"[video]\" -map 2:a -shortest -vcodec h264_nvenc -cq:v 51 -preset:v default \"C:\\TEST\\GenerateClipNvencCQAt51PresetDefault.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipNvencCQAt1PresetP7()
        {
            // Changes CQ and preset
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -map \"[video]\" -map 2:a -shortest -vcodec h264_nvenc -cq:v 1 -preset:v p7 \"C:\\TEST\\GenerateClipNvencCQAt1PresetP7.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipNvencCQAt51PresetP7()
        {
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -map \"[video]\" -map 2:a -shortest -vcodec h264_nvenc -cq:v 51 -preset:v p7 \"C:\\TEST\\GenerateClipNvencCQAt51PresetP7.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipNvencCQAt1PresetDefaultProfileBaseline()
        {
            // Changes CQ, preset and profile
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -map \"[video]\" -map 2:a -shortest -vcodec h264_nvenc -cq:v 1 -preset:v default " +
                "-profile:v baseline \"C:\\TEST\\GenerateClipNvencCQAt1PresetDefaultProfileBaseline.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipNvencCQAt51PresetDefaultProfileBaseline()
        {
            // Changes CQ, preset and profile
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -map \"[video]\" -map 2:a -shortest -vcodec h264_nvenc -cq:v 51 -preset:v default " +
                "-profile:v basline \"C:\\TEST\\GenerateClipNvencCQAt51PresetDefaultProfileBaseline.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipNvencCQAt1PresetP7ProfileBaseline()
        {
            // Changes CQ, preset and profile
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -map \"[video]\" -map 2:a -shortest -vcodec h264_nvenc -cq:v 1 -preset:v p7 " +
                "-profile:v basline \"C:\\TEST\\GenerateClipNvencCQAt1PresetP7ProfileBaseline.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipNvencCQAt51PresetP7ProfileBaseline()
        {
            // Changes CQ, preset and profile
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -map \"[video]\" -map 2:a -shortest -vcodec h264_nvenc -cq:v 51 -preset:v p7 " +
                "-profile:v baseline \"C:\\TEST\\GenerateClipNvencCQAt51PresetP7ProfileBaseline.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipNvencCQAt1PresetDefaultProfileHigh()
        {
            // Changes CQ, preset and profile
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -map \"[video]\" -map 2:a -shortest -vcodec h264_nvenc -cq:v 1 -preset:v default " +
                "-profile:v baseline \"C:\\TEST\\GenerateClipNvencCQAt1PresetDefaultProfileHigh.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipNvencCQAt51PresetDefaultProfileHigh()
        {
            // Changes CQ, preset and profile
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -map \"[video]\" -map 2:a -shortest -vcodec h264_nvenc -cq:v 51 -preset:v default " +
                "-profile:v basline \"C:\\TEST\\GenerateClipNvencCQAt51PresetDefaultProfileHigh.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipNvencCQAt1PresetP7ProfileHigh()
        {
            // Changes CQ, preset and profile
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -map \"[video]\" -map 2:a -shortest -vcodec h264_nvenc -cq:v 1 -preset:v p7 " +
                "-profile:v basline \"C:\\TEST\\GenerateClipNvencCQAt1PresetP7ProfileHigh.mp4\"";

            CreateProcess(arguments);
        }

        [Benchmark]
        public void GenerateClipNvencCQAt51PresetP7ProfileHigh()
        {
            // Changes CQ, preset and profile
            var arguments = "-y -i \"C:\\Repos\\Benchmark\\foreground.tmp\" -stream_loop -1 " +
                "-i \"C:\\Repos\\Benchmark\\background.mp4\" -i \"C:\\Repos\\Benchmark\\audio-short.mp3\" " +
                "-filter_complex \"[1:v]scale = 3840:2160[bg];[0:v]scale = 3840:2160[png];[bg][png]overlay[video]\" " +
                "-s 3840:2160 -map \"[video]\" -map 2:a -shortest -vcodec h264_nvenc -cq:v 51 -preset:v p7 " +
                "-profile:v baseline \"C:\\TEST\\GenerateClipNvencCQAt51PresetP7ProfileHigh.mp4\"";

            CreateProcess(arguments);
        }

        public void CreateProcess(string arguments)
        {
            var fileName = "C:\\Repos\\Benchmark\\ffmpeg.exe";

            Process p = new Process();
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            p.StartInfo.FileName = fileName;
            p.StartInfo.Arguments = arguments;
            p.EnableRaisingEvents = true;
            p.Start();
            p.BeginOutputReadLine();
            p.BeginErrorReadLine();
            p.WaitForExit();
            p.Close();
        }
    }
}
