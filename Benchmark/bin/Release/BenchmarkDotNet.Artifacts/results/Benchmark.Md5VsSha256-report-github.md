``` ini

BenchmarkDotNet=v0.13.1, OS=Windows 10.0.19044.1645 (21H2)
Intel Core i7-6700HQ CPU 2.60GHz (Skylake), 1 CPU, 8 logical and 4 physical cores
  [Host]     : .NET Framework 4.8 (4.8.4470.0), X86 LegacyJIT  [AttachedDebugger]
  DefaultJob : .NET Framework 4.8 (4.8.4470.0), X86 LegacyJIT


```
| Method |      Mean |    Error |   StdDev |
|------- |----------:|---------:|---------:|
| Sha256 | 117.55 μs | 2.343 μs | 2.301 μs |
|    Md5 |  22.81 μs | 0.291 μs | 0.272 μs |
