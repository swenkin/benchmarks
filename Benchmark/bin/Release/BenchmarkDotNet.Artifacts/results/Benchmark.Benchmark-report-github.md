``` ini

BenchmarkDotNet=v0.13.1, OS=Windows 10.0.19044.1645 (21H2)
Intel Core i7-6700HQ CPU 2.60GHz (Skylake), 1 CPU, 8 logical and 4 physical cores
  [Host]     : .NET Framework 4.8 (4.8.4470.0), X86 LegacyJIT  [AttachedDebugger]
  DefaultJob : .NET Framework 4.8 (4.8.4470.0), X86 LegacyJIT


```
|                             Method |    Mean |    Error |   StdDev | Rank | SizeAfterCreation |
|----------------------------------- |--------:|---------:|---------:|-----:|------------------:|
| GenerateClipCRFAt15PresetUltraFast | 2.099 s | 0.0457 s | 0.1311 s |    1 |              28MB |
| GenerateClipCRFAt10PresetUltraFast | 2.178 s | 0.0434 s | 0.0815 s |    2 |              28MB |
| GenerateClipCRFAt15PresetSuperFast | 3.410 s | 0.0630 s | 0.0589 s |    3 |              28MB |
| GenerateClipCRFAt10PresetSuperFast | 3.790 s | 0.0358 s | 0.0317 s |    4 |              28MB |
